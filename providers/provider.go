package providers

import "gitlab.com/PuKoren/go-assignment/engine"

type Provider interface {
    GetKnightRepository() engine.KnightRepository
    Close()
    Clean() error
    NewProvider() *Provider
}
