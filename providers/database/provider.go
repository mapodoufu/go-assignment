package database

import (
    "os"
    "log"
    "fmt"
    "strings"
    "runtime"
    "database/sql"
    "path/filepath"

    "github.com/golang-migrate/migrate"
    _ "github.com/go-sql-driver/mysql"
    _ "github.com/golang-migrate/migrate/database/mysql"
    _ "github.com/golang-migrate/migrate/source/file"

    "gitlab.com/PuKoren/go-assignment/engine"
)

type Provider struct {
    db *sql.DB
    knightRepo *knightRepository
}

var (
    _, b, _, _ = runtime.Caller(0)
    basepath   = filepath.Dir(b)
)

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return provider.knightRepo
}

func (provider *Provider) Close() {
    provider.db.Close()
}

func (provider *Provider) Clean() {
    provider.knightRepo.Clean()
}

func NewProvider() *Provider {
    connString := os.Getenv("DB_CONN_STRING")
    if connString == "" {
        connString =  "mysql://root:root@tcp(127.0.0.1:3306)/arena"
    }

    db, err := sql.Open("mysql", strings.TrimPrefix(connString, "mysql://"))

    if err != nil {
        log.Fatal(err)
    }

    migrationsPath := fmt.Sprintf("file://%s/../../migrations", basepath)
    m, err := migrate.New(migrationsPath, connString)

    if err != nil {
        log.Fatal(err)
    }

    m.Steps(2)

	return &Provider{db: db, knightRepo: &knightRepository{ db: db } }
}
