package domain

import (
    "math/rand"
    "math"
    "time"
)

type Arena struct{}

func (arena *Arena) Fight(fighter1 Fighter, fighter2 Fighter) *Fighter {
    // if stricly equal power, we return nil (no luck involved)
    // to match the requirements of tests
    if fighter1.GetPower() == fighter2.GetPower() {
        return nil
    }

    // we add a random factor for luck if strengths are not equal
    // because it is important to have luck in a fantasy game 
    rand.Seed(time.Now().UTC().UnixNano())

    // this should not add more than 1 maximum
    // so power still matters !
    randValue1 := math.Round(rand.Float64())
    randValue2 := math.Round(rand.Float64())

	if (fighter1.GetPower() + randValue1 > fighter2.GetPower() + randValue2) {
        return &fighter1
    } else if (fighter2.GetPower() + randValue2 > fighter1.GetPower() + randValue1) {
        return &fighter2
    }

    return nil
}
