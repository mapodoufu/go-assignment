# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
I'm lacking experience on the language as it was my first time using Go, so I may be wrong about the following part.

I think that the initial project structure looks good, but I sometimes had the feeling that some parts of the code were overlapping, and that it could actually be simpler.
However I liked the fact that the adapter, domain and providers were separated. Is the engine part acting like a glue between domain and providers ?

I noticed that some tests were written with previous tests execution dependency: checking if there is two knights after inserting knights in previous tests: this could lead to breaking the test suite if we add a new test that insert a knight. I think each test should be independant. I tried to do that on the providers tests by cleaning the DB between each test.


 - **What you will improve from your solution ?**
I think my solution is quite messy and probably over-complicated because I don't know Go. So I would first continue learning the language and then simplify the code and refactor it.

Then my user input checking and model validation is bad. From what I seen, I think it is possible to use annotations and some libs to validate input properly and reduce the amount of code of this project.

Then my error handling through the whole app is not consistent and does not cover all failing cases. So I will rework that to have a stable service (this is related to the refactor mentioned above).


 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
I don't have a strong opinion on it. I think sometimes boundary can be reduced to a very simple role, like making an arithmetic operation. But some other times role will be very difficult to reduce and the micro-service will grow to a tentacular service - I think it can happen sometimes for adapters to external services, or for services with a lot of business rules and events (as for a billing service, sometimes the billing can react to many different events from all the stack and have very complex rules).

It may also depends on the architecture of the whole stack, the communication methods, the performances requirements of the service... There is many things in a project that can change my vision of a micro-service boundary, so it is difficult for me to give a clear answer.


 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**

### SQL
I would use SQL for an application where data integrity and accuracy, and where ACID transactions are critical for production - for example services related to billing or financial transactions.

### NoSQL
NoSQL would be useful for high I/O operations and evolving models, where some concurrency errors or data loss/outdated are tolerable (it should be minimal), like for services involving configurations, user profiles, usage logs, statistics...

As for NoSQL, I would also promote it on the early stages of a company even for critical data (if the team is senior and have good architectural skills) because it makes development of the product very fast - so for POC or MVP I would go for it.

MongoDB aggregation is very powerful to extract data projections and make complex query on the documents, so duplicating data from SQL to MongoDB can also be useful in some cases (I mean that there is some cases where SQL and NoSQL can be used together).


### Key-value
I would use key-value stores mostly for caching or accessing data that is easy to parse and store, and requires very fast read speed. It would also be useful to share data between different instances of a single service (I think it is a typical usage of Redis)
