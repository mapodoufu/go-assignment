package engine

import (
	"errors"

	"gitlab.com/PuKoren/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) *domain.Knight {
	fighter := engine.knightRepository.Find(ID)

	// I would like to return nil if no Knight is available, and handle 
	// the not found case in the router handler, as I would like error-facing errors
	// to be handled by the http adapter (so engine stay light, as I can see case
	// where GetKnight could be called and return value expected to be null
	// as to check if there is already a knight with this ID, in this case it is not
	// considered to be an error
	// if fighter == nil {
	// 	return nil, errors.New(fmt.Sprintf("fighter with ID '%s' not found!", ID))
	// }

	return fighter
}

func (engine *arenaEngine) AddKnight(knight domain.Knight) (*domain.Knight, error) {
	return engine.knightRepository.Save(knight)
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) (*domain.Fighter, error) {
	knight1 := engine.GetKnight(fighter1ID)
	if knight1 == nil {
		return nil, errors.New("Fighter 1 not found")
	}

	knight2 := engine.GetKnight(fighter2ID)
	if knight2 == nil {
		return nil, errors.New("Fighter 2 not found")
	}

	return engine.arena.Fight(knight1, knight2), nil
}
