package engine

import "gitlab.com/PuKoren/go-assignment/domain"

type Engine interface {
	GetKnight(ID string) *domain.Knight
	ListKnights() []*domain.Knight
	AddKnight(knight domain.Knight) (*domain.Knight, error)
	Fight(fighter1ID string, fighter2ID string) (*domain.Fighter, error)
}

type KnightRepository interface {
	Find(ID string) *domain.Knight
	FindAll() []*domain.Knight
	Save(knight domain.Knight) (*domain.Knight, error)
}

type DatabaseProvider interface {
	GetKnightRepository() KnightRepository
}

type arenaEngine struct {
	arena            *domain.Arena
	knightRepository KnightRepository
}

func NewEngine(db DatabaseProvider) Engine {
	return &arenaEngine{
		arena:            &domain.Arena{},
		knightRepository: db.GetKnightRepository(),
	}
}
