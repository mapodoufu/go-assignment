package http

import (
    "fmt"
)

type CustomError struct {
    Code int `json:"code"`
    Message string `json:"message"`
}

func NewError(code int, err error) CustomError {
    return CustomError{ Code: code, Message: fmt.Sprintf("%v", err) }
}
