package http

import (
    "encoding/json"
    "net/http"
    "context"
    "errors"
    "time"
    "log"
    "fmt"

    "github.com/gorilla/mux"

	"gitlab.com/PuKoren/go-assignment/engine"
    "gitlab.com/PuKoren/go-assignment/domain"
)

type HTTPAdapter struct{
    server *http.Server
}

type Handler struct {
    e engine.Engine
}

func (adapter *HTTPAdapter) Start() {
    log.Print("Starting to serve on port :8080")
    
    go func() {
        if err := adapter.server.ListenAndServe(); err != nil {
            log.Println(err)
        }
    }()
}

func (adapter *HTTPAdapter) Stop() {
    log.Println("Received shutdown signal, shutting down now...")

    ctx, cancel := context.WithTimeout(context.Background(),  5 * time.Second)
    defer cancel()
    adapter.server.Shutdown(ctx)

    log.Println("Done")
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
    router := mux.NewRouter()

    handler := Handler{e}

    BindHandlers(router, &handler)

    server := &http.Server{
        Addr:         "0.0.0.0:8080",
        WriteTimeout: time.Second * 15,
        ReadTimeout:  time.Second * 15,
        IdleTimeout:  time.Second * 60,
        Handler: router,
    }

	return &HTTPAdapter{server}
}

func BindHandlers(router *mux.Router, handler *Handler) {
    router.HandleFunc("/knight", handler.ListKnights).Methods("GET")
    router.HandleFunc("/knight/{id}", handler.GetKnight).Methods("GET")
    router.HandleFunc("/knight", handler.CreateKnight).Methods("POST")
    router.HandleFunc("/{id1}/fight/{id2}", handler.Fight).Methods("POST")
}

func (handler *Handler) ListKnights (w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(handler.e.ListKnights())
}

func (handler *Handler) GetKnight(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    params := mux.Vars(r)

    knight := handler.e.GetKnight(params["id"])

    if knight == nil {
        w.WriteHeader(http.StatusNotFound)
        json.NewEncoder(w).Encode(
            NewError(http.StatusNotFound,
                errors.New(fmt.Sprintf("Knight #%s not found.", params["id"]))))
        return
    }

    json.NewEncoder(w).Encode(knight)
}

func (handler *Handler) CreateKnight(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    var knight = domain.Knight{}

    err := json.NewDecoder(r.Body).Decode(&knight)

    // it is important to prevent user from providing his own value for id
    // I don't know the proper way of doing this using json anotations in model
    // so I do it here (when I use json: "-" it works but Marshaling prevent it to
    // being send back to user)
    knight.Id = ""

    if err == nil {
        err = knight.Validate()
    }

    if err != nil {
        w.WriteHeader(http.StatusBadRequest)
        json.NewEncoder(w).Encode(NewError(http.StatusBadRequest, err))
        return
    }

    refreshedKnight, err := handler.e.AddKnight(knight)

    if err != nil {
        w.WriteHeader(http.StatusInternalServerError)
        json.NewEncoder(w).Encode(NewError(http.StatusInternalServerError, err))
        return
    }

    w.WriteHeader(http.StatusCreated)

    json.NewEncoder(w).Encode(refreshedKnight)
}

func (handler *Handler) Fight(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    params := mux.Vars(r)

    if params["id1"] == "" || params["id2"] == "" {
        w.WriteHeader(http.StatusBadRequest)
        json.NewEncoder(w).Encode(NewError(http.StatusInternalServerError, errors.New("Must provide fighters IDs")))
        return
    }

    winner, err := handler.e.Fight(params["id1"], params["id2"])

    if (err != nil) {
        w.WriteHeader(http.StatusBadRequest)
        json.NewEncoder(w).Encode(NewError(http.StatusBadRequest, err))
        return
    }
    
    if winner != nil {
        json.NewEncoder(w).Encode(winner)
    }
}
